
build-docker-images:
	cd images/nodejs10-grunt && make build
	cd images/python3.8-base && make build
	cd images/rabbitmq && make build
	cd images/redis && make build


make-init-server:
	cd raw-server && ./init-environment.sh
