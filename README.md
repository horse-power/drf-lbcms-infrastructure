
## Разворачивание окружения на сервере


```bash
    git clone https://bitbucket.org/horse-power/drf-lbcms-infrastructure.git
    cd drf-lbcms-infrastructure/raw-server
    ./init-environment.sh

```


## Разворачивание окружения на dev-машине

1. установите docker, make, git


2. запустите:

```bash

    git clone https://bitbucket.org/horse-power/drf-lbcms-infrastructure.git
    cd drf-lbcms-infrastructure
    make build-docker-images

```
