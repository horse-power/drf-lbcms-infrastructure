try:
    from pyngrok import ngrok
    if hasattr(ngrok, "ensure_ngrok_installed"):
        ngrok.ensure_ngrok_installed(ngrok.conf.DEFAULT_NGROK_PATH)
    else:
        ngrok.install_ngrok()
except Exception as error:
    print("Failed to find pyngrok!")
