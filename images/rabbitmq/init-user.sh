#!/bin/sh

# https://stackoverflow.com/questions/30747469/how-to-add-initial-users-when-starting-a-rabbitmq-docker-container

# Create Rabbitmq user
rabbitmq-server &
sleep 15

rabbitmqctl add_user default_user default_password 2>/dev/null
rabbitmqctl add_vhost bot 2>/dev/null
rabbitmqctl set_permissions -p bot default_user ".*" ".*" ".*"

sleep 1
rabbitmqctl stop
killall rabbitmq-server
