#!/bin/sh

# https://stackoverflow.com/questions/30747469/how-to-add-initial-users-when-starting-a-rabbitmq-docker-container

# Create Rabbitmq user
#( sleep 5 ; \
#rabbitmqctl add_user default_user default_password 2>/dev/null || true ; \
#rabbitmqctl add_vhost bot 2>/dev/null || true ; \
#rabbitmqctl set_permissions -p bot default_user ".*" ".*" ".*" || true) &


# $@ is used to pass arguments to the rabbitmq-server command.
# For example if you use it like this: docker run -d rabbitmq arg1 arg2,
# it will be as you run in the container rabbitmq-server arg1 arg2
rabbitmq-server $@
# 1> /dev/null
