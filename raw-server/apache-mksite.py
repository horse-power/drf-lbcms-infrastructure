#!/usr/bin/python3
import os, yaml
from sys import exit, argv

MAIN_DOMAIN = "gls-sites.cc.ua"
PATH_TO_APACHE_CONF_DIR = "/etc/apache2/sites-available"

APP_DIR = os.path.dirname(argv[1])
DJANGO_APP_PREFIX = 'backend'


def mkdir(*args):
    try:
        os.mkdir(os.path.join(*args))
    except:
        pass

if len(argv) < 2:
    print("Please, provide path to yaml file (docker-compose.yml)")


with open(argv[1]) as f:
    docker_conf = yaml.load(f, Loader=yaml.SafeLoader)

if not os.path.exists(os.path.join(APP_DIR, DJANGO_APP_PREFIX)):
    DJANGO_APP_PREFIX = 'web'


SITE_CONF = """
# generated with infrastructure/raw-server/apache-mksite.py

<VirtualHost *:80>
    ServerName {_serverBucket}.{mainDomain}
    ServerAlias www.{_serverBucket}.{mainDomain}
    {serverAlias}
    {djangoConf}

    Alias /client/ /static_data/{serverBucket}/
    Alias /storage/lbcms-container-{serverBucket}/ /media_data/{serverBucket}/

    <Directory /media_data/{serverBucket}>
        Require all granted
    </Directory>
    <Directory /static_data/{serverBucket}>
        Require all granted
    </Directory>

    ErrorLog ${{APACHE_LOG_DIR}}/{serverBucket}/error.log
    CustomLog ${{APACHE_LOG_DIR}}/{serverBucket}/access.log combined

</VirtualHost>


<VirtualHost *:443>
    ServerName {_serverBucket}.{mainDomain}
    ServerAlias www.{_serverBucket}.{mainDomain}
    {serverAlias}
    {djangoConfHttps}

    Alias /client/ /static_data/{serverBucket}/
    Alias /storage/lbcms-container-{serverBucket}/ /media_data/{serverBucket}/

    <Directory /media_data/{serverBucket}>
        Require all granted
    </Directory>
    <Directory /static_data/{serverBucket}>
        Require all granted
    </Directory>

    ErrorLog ${{APACHE_LOG_DIR}}/{serverBucket}/error.log
    CustomLog ${{APACHE_LOG_DIR}}/{serverBucket}/access.log combined

</VirtualHost>
"""

DJANGO_CONF = """
    # ========== django
    ProxyPass /.well-known/acme-challenge/ !
    ProxyPass /client/ !
    ProxyPass /storage/lbcms-container-{serverBucket}/ !
    ProxyPass / unix:/run/uwsgi/{serverBucket}-{djangoAppPrefix}.sock|uwsgi://hostname-to-ignore/
    # =========== end django
"""
DJANGO_CONF_HTTPS = """
    # ========== django
    ProxyPass /.well-known/acme-challenge/ !
    ProxyPass /client/ !
    ProxyPass /storage/lbcms-container-{serverBucket}/ !
    ProxyPass / unix:/run/uwsgi/{serverBucket}-{djangoAppPrefix}.sock|uwsgi://hostname-to-ignore/
    # =========== end django
"""

server_alias = """
    ServerAlias {serverName}
    ServerAlias www.{serverName}

"""

serverBucket = os.path.basename(os.path.dirname(argv[1]))


if os.path.exists(os.path.join(PATH_TO_APACHE_CONF_DIR, serverBucket + '.conf')):
    override = input('Conf file already exists. Override (y - yes, n - no)? ')
    if override.lower() in ['n', 'no']:
        exit()

serverName = input('Type server name (without www. or http:// ): ')
if serverName:
    serverAlias = server_alias.format(
        serverName=serverName
    )
else:
    # пытаемся взять из docker_conf
    serverAlias = docker_conf['services'][DJANGO_APP_PREFIX]['environment'].get('LETSENCRYPT_HOST', '')
    if '$' in serverAlias:
        serverAlias = ''

with open(os.path.join(PATH_TO_APACHE_CONF_DIR, serverBucket + '.conf'), 'w') as f:
    _serverBucket = serverBucket.replace('_','-')
    djangoConf = DJANGO_CONF.format(
        serverName=serverName,
        serverBucket=serverBucket,
        _serverBucket=_serverBucket,
        mainDomain=MAIN_DOMAIN,
        djangoAppPrefix=DJANGO_APP_PREFIX
    )
    djangoConfHttps = DJANGO_CONF_HTTPS.format(
        serverName=serverName,
        serverBucket=serverBucket,
        _serverBucket=_serverBucket,
        mainDomain=MAIN_DOMAIN,
        djangoAppPrefix=DJANGO_APP_PREFIX
    )
    print(
        SITE_CONF.format(
            serverName=serverName,
            serverBucket=serverBucket,
            _serverBucket=_serverBucket,
            serverAlias=serverAlias,
            mainDomain=MAIN_DOMAIN,
            djangoConf=djangoConf,
            djangoConfHttps=djangoConfHttps,
            dockerComposeFile=os.path.basename(argv[1]),
            djangoAppPrefix=DJANGO_APP_PREFIX
        )
    )
    f.write(
        SITE_CONF.format(
            serverName=serverName,
            serverBucket=serverBucket,
            _serverBucket=_serverBucket,
            serverAlias=serverAlias,
            mainDomain=MAIN_DOMAIN,
            djangoConf=djangoConf,
            djangoConfHttps=djangoConfHttps,
            dockerComposeFile=os.path.basename(argv[1]),
            djangoAppPrefix=DJANGO_APP_PREFIX
        )
    )


for d in ['/db_data', '/media_data', '/static_data']:
    mkdir(d)
    mkdir(d, serverBucket)
mkdir('/var/log/apache2', serverBucket)
mkdir('/var/log/uwsgi')


for d in ['/var/www/', '/db_data/', '/media_data/', '/static_data/']:
    os.system('chgrp -R www-data ' + d + serverBucket)
    os.system('chmod g+rwx -R ' + d + serverBucket)

os.system('a2ensite ' + serverBucket)
os.system('apachectl -k graceful')
