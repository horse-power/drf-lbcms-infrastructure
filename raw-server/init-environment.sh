#!/bin/bash
rm -rf /var/lib/apt/lists/* && apt-get clean

# swap
fallocate -l 1G /swapfile
dd if=/dev/zero of=/swapfile bs=1024 count=1048576
chmod 600 /swapfile
mkswap /swapfile
swapon /swapfile
echo "/swapfile swap swap defaults 0 0" >> /etc/fstab

# packages
apt-get update
apt-get install -y apache2 make git libapache2-mod-wsgi-py3 libapache2-mod-proxy-uwsgi
# или первый или второй вариант запустится
apt-get install -y uwsgi-plugin-python3 python3-virtualenv || apt-get install -y uwsgi-plugin-python python-virtualenv

apt-get install -y git python3-pip make python3-dev libffi-dev libssl-dev \
        libtiff5-dev libjpeg8-dev zlib1g-dev \
        libfreetype6-dev liblcms2-dev libwebp-dev libharfbuzz-dev libfribidi-dev \
        tcl8.6-dev tk8.6-dev python3-tk p7zip-full gettext libmysqlclient-dev redis-server


apt install -y ./*.deb
apt-get clean
pip3 install --upgrade setuptools
pip3 install wheel
pip3 install -r ./requirements.py.txt --no-cache-dir || pip3 install -r ./requirements.py.txt --process-dependency-links --no-cache-dir


#systemctl enable redis-server.service

# uwsgi
mkdir -p /var/log/uwsgi
chown www-data /var/log/uwsgi

if [ -e /usr/local/bin/uwsgi ]
then
    cp ./emperor.uwsgi.service.local /etc/systemd/system/emperor.uwsgi.service
else
    cp ./emperor.uwsgi.service.global /etc/systemd/system/emperor.uwsgi.service
fi
systemctl enable emperor.uwsgi.service
systemctl daemon-reload
systemctl start emperor.uwsgi.service


# apache
echo "cd /var/www" >> ~/.bashrc
rm /etc/apache2/sites-enabled/000-default.conf || true
rm -fr /var/www/html || true
cp ./apache-mksite.py /var/www/mksite
chmod +x /var/www/mksite

cat ./expire-apache.conf >> /etc/apache2/apache2.conf

a2enmod ssl
a2enmod proxy
a2enmod proxy_http
a2enmod proxy_balancer
a2enmod proxy_uwsgi
a2enmod expires
a2enmod headers
systemctl restart apache2
apachectl -k graceful

# certbot
snap install core; snap refresh core
snap install --classic certbot
ln -s /snap/bin/certbot /usr/bin/certbot
#apt-get install -y software-properties-common
#add-apt-repository universe
#add-apt-repository ppa:certbot/certbot
#apt-get update
#apt-get install -y certbot python3-certbot-apache

certbot --apache

journalctl --vacuum-size=100M
echo "SystemMaxUse=100M" > /etc/systemd/journald.conf
systemctl restart systemd-journald.service
